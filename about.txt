Заполняем базу:
    psql -U jsp -d testbase

    CREATE SEQUENCE city_ids;
    CREATE TABLE cities (id INTEGER PRIMARY KEY DEFAULT NEXTVAL('city_ids'), name CHAR(64), description CHAR(64));

    CREATE SEQUENCE user_ids;
    CREATE TABLE users (id INTEGER PRIMARY KEY DEFAULT NEXTVAL('user_ids'), name CHAR(64), secondName CHAR(64),surname CHAR(64),city_id bigint REFERENCES cities(id));

    CREATE SEQUENCE car_ids;
    CREATE TABLE cars (id INTEGER PRIMARY KEY DEFAULT NEXTVAL('car_ids'), license_plate CHAR(64), model CHAR(64), year bigint, user_id bigint REFERENCES users(id));

    INSERT INTO cities (name,description) values('Новосибирск','мой город');
    INSERT INTO cities (name,description) values('Москва','столица');
    INSERT INTO cities (name,description) values('Санкт-Петербург','это же Ленинград');
    INSERT INTO cities (name,description) values('Ростов-на-Дону','город папа');
    INSERT INTO cities (name,description) values('Владивосток','далеко');
    INSERT INTO cities (name,description) values('Пермь','это город');
    INSERT INTO cities (name,description) values('Красноярск','столбы');
    INSERT INTO cities (name,description) values('Барнаул','почти Алтай');
    INSERT INTO cities (name,description) values('Горно-Алтайск','за ним горы');
    INSERT INTO cities (name,description) values('Иркутск','город');

    INSERT INTO users (name,secondname,surname,city_id) values('Юрий','Венедиктович','Пронин',2);
    INSERT INTO users (name,secondname,surname,city_id) values('Виктор','Харитонович','Андреев',2);
    INSERT INTO users (name,secondname,surname,city_id) values('Семён','Васильевич','Слепцов',1);
    INSERT INTO users (name,secondname,surname,city_id) values('Аркадий','Петрович','Паровозов',3);
    INSERT INTO users (name,secondname,surname,city_id) values('Эдуард','Владимирович','Мостовых',1);
    INSERT INTO users (name,secondname,surname,city_id) values('Сергей','Михайлович','Ковалёв',10);
    INSERT INTO users (name,secondname,surname,city_id) values('Сергей','Михайлович','Расторопшин',9);
    INSERT INTO users (name,secondname,surname,city_id) values('Сергей','Михайлович','Ковалёв',8);
    INSERT INTO users (name,secondname,surname,city_id) values('Виктор','Михайлович','Ковалёв',7);

    INSERT INTO cars (license_plate,model,year,user_id) values('M001MO77','BMW X6',2017,1);
    INSERT INTO cars (license_plate,model,year,user_id) values('M001MO77','BMW X6',2017,1);                                       
    INSERT INTO cars (license_plate,model,year,user_id) values('X001XX77','Mercedes-Benz GLC 300',2018,3);
    INSERT INTO cars (license_plate,model,year,user_id) values('X002XX77','Mercedes-Benz C238',2017,3);
    INSERT INTO cars (license_plate,model,year,user_id) values('X003XX77','Range Rover Sport',2016,3);
    INSERT INTO cars (license_plate,model,year,user_id) values('Y555XX77','Porsche Panamera S',2015,4);
    INSERT INTO cars (license_plate,model,year,user_id) values('K555KK177','Porsche Cayenne',2018,4);
    INSERT INTO cars (license_plate,model,year,user_id) values('Y238XO54','Lada Sedan',2005,5);
    INSERT INTO cars (license_plate,model,year,user_id) values('O0780O78','Volvo XC60',2005,6);
    INSERT INTO cars (license_plate,model,year,user_id) values('A078AA78','Volvo V40',2017,6);
    INSERT INTO cars (license_plate,model,year,user_id) values('T054OT54','Citroen C4',2017,7);
    INSERT INTO cars (license_plate,model,year,user_id) values('M136AT36','УАЗ Патриот',2014,8);
    INSERT INTO cars (license_plate,model,year,user_id) values('M125EP25','Toyota Crown',2014,9);
    INSERT INTO cars (license_plate,model,year,user_id) values('M125EP125','Toyota Celica',2014,9);
    INSERT INTO cars (license_plate,model,year,user_id) values('P007PP159','BMW X5',2010,10);
    INSERT INTO cars (license_plate,model,year,user_id) values('У007КP38','Toyota Celica',2001,11);
    INSERT INTO cars (license_plate,model,year,user_id) values('P005OT22','Lada Sedan',2015,12);
    INSERT INTO cars (license_plate,model,year,user_id) values('P005OM22','УАЗ Патриот',2015,13);
    INSERT INTO cars (license_plate,model,year,user_id) values('K001MM24','Honda Accord',2005,14);
    INSERT INTO cars (license_plate,model,year,user_id) values('K555MM24','Subaru Impreza',2007,14);



