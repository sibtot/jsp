package servlets;

import entities.Car;
import entities.User;
import org.apache.log4j.Logger;
import utils.Converter;
import utils.Postgres;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SearchServlet extends HttpServlet {


    final static Logger logger = Logger.getLogger(SearchServlet.class);
    private String errorText="";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String surname ="";
        String name = "";
        String secondName ="";
        String city ="";
        String car = "";

        //Поля для расширенного поиска
        String licensePlate = "";
        String year = "";
        String model = "";
        String threeDigits = "";



        //Получаем данные из JS
        try {
            //Параметры основного запроса
            surname = req.getParameter("surname");      surname=surname.trim();
            name = req.getParameter("name");            name=name.trim();
            secondName = req.getParameter("secondName");secondName=secondName.trim();
            city = req.getParameter("city");            city=city.trim();
            car = req.getParameter("car");              car=car.trim();
        }catch(Exception e){
            errorText="Запрос без параметров. ";
            logger.debug(errorText,e);
        }


        Postgres psql=new Postgres();

        if(psql.getErrorText().length()==0) {
            List<User> users = new ArrayList<>();

            String html = "";

            //Если все поля пустые, то выводим полный список пользователей
            if ((surname == null || surname.isEmpty()) &&
                    (name == null || name.isEmpty()) &&
                    (secondName == null || secondName.isEmpty()) &&
                    (city == null || city.isEmpty()) &&
                    (car == null || car.isEmpty())) {

                users = psql.userSearch(); //Выводим список всех пользователей

            } else {

                if ((car == null || car.isEmpty())) {//Поиск без авто, в поле авто пусто

                    users = psql.userSearch(surname, name, secondName, city);

                } else { //начинаем поиск по авто

                    car = car.trim();
                    car = car.replaceAll("\\s+", " ");
                    String[] carsStr = car.split(" ");

                    //Пример запроса "Subaru Forester 2003 y888xo54 555"
                    for (String s : carsStr) { //определяем к какому типу объекта можно отнести слова в строке: номер, модель, год

                        s = s.toUpperCase();

                        if (s.matches("^[0-9]{4}$")) {

                            int digitYear=Integer.parseInt(s);
                            int localYear=Integer.parseInt(new SimpleDateFormat("YYYY", new Locale("ru")).format(new Date()));

                            if((digitYear>1885) && (digitYear<=localYear)) {//Проверяем что это могу быть года постройки серийных автомобилей
                                car = car.replace(s, "");
                                year = s;
                            }

                            model = model + " " + s; //Так же оставляем возможность поиска для модели

                        } else if (s.matches("^[A,B,E,K,M,H,O,P,C,T,Y,X]{1}[0-9]{3}[A,B,E,K,M,H,O,P,C,T,Y,X]{2}[0-9]{2,3}$") ||  //Латинские символы автономера
                                s.matches("^[А,В,Е,К,М,Н,О,Р,С,Т,У,Х]{1}[0-9]{3}[А,В,Е,К,М,Н,О,Р,С,Т,У,Х]{2}[0-9]{2,3}$")) {    //Русские символы

                            car = car.replace(s, "");

                            s = Converter.ruToLat(s);//заменяем русские буквы на латинские перед поиском в базе
                            licensePlate = s;

                        } else if (s.matches("^[-а-яА-ЯёЁa-zA-Z0-9 ]+$")) {//Возможно модель автомобиля

                            model = model + " " + s;

                            if (s.matches("^[A,B,E,K,M,H,O,P,C,T,Y,X]{0,1}[0-9]{0,3}[A,B,E,K,M,H,O,P,C,T,Y,X]{0,2}[0-9]{0,3}$") ||
                                    s.matches("^[А,В,Е,К,М,Н,О,Р,С,Т,У,Х]{0,1}[0-9]{0,3}[А,В,Е,К,М,Н,О,Р,С,Т,У,Х]{0,2}[0-9]{0,3}$")) {//Возможно часть номера автомобиля (в первом случае латиница, во втором русские)

                                threeDigits = s;

                            }
                        } else {

                            html += " Не могу определить критерии поиска для слова " + s + ".";

                        }
                    }


                    //Как будем искать:
                    Set<Car> cars = new HashSet<>();
                    Set<Long> ids = new HashSet<>();

                    if (licensePlate != null &&  licensePlate.length() > 0) {

                        html += " В строке запроса указан номер автомобиля " + licensePlate + ". Остальные критерии поиска не учитываются.";

                        cars = psql.carSearchByLicensePlate(licensePlate, "");

                        for (Car carFind : cars) {
                            ids.add(carFind.getUserId());
                        }

                    } else { //Если в запросе нет номера, то ищем по другим параметрам


                        if (year.length() > 0 && year != null) {

                            model = model.replace(year, "");
                            model=model.trim();

                            String str = model.length() > 0 ? " и моделе авто " + model + "." : ".";
                            html += " Ищем по году выпуска " + year + str;


                            cars = psql.carSearchByYearModel(year, model);

                            for (Car carFind : cars) {
                                ids.add(carFind.getUserId());
                            }

                            if (threeDigits.length() > 0) {//Есть подозрение на часть номера

                                model = model.replace(threeDigits, ""); //Вырезаем из модели тогда слово похожее  на номер

                                String pLicensePlate = Converter.ruToLat(threeDigits);//Преобразуем в латиницу.

                                html += " Пробуем искать по моделе " + model + "и символам " + pLicensePlate + " в гос.номере, выпущенным в " + year + ".";

                                cars = psql.carSearchByYearLicensePlateModel(year, pLicensePlate, model);

                                for (Car carFind : cars) {
                                    ids.add(carFind.getUserId());
                                }
                            }

                        } else if (model.length() > 0) { //только модель

                            html += " Ищем по моделе автомобиля " + model + ".";

                            cars = psql.carSearchByModel(model);

                            for (Car carFind : cars) {
                                ids.add(carFind.getUserId());
                            }

                            if (threeDigits.length() > 0) {

                                model = model.replace(threeDigits, "");
                                model=model.trim();

                                String pLicensePlate = Converter.ruToLat(threeDigits);

                                String str = model.length() > 0 ? " и моделе авто " + model + "." : ".";
                                html += " Возможно символы " + pLicensePlate + " есть в гос.номере автомобиля. Делаем ещё поиск по гос.номеру " + pLicensePlate + str;
                                cars = psql.carSearchByLicensePlate(pLicensePlate, model);

                                for (Car carFind : cars) {
                                    ids.add(carFind.getUserId());
                                }
                            }

                        }

                    }//licensePlate

                    for (long id : ids) { //TODO: можно переделать на поиск по where id in, формировать строку с id вида (1,3,5,7) и передавать её в качестве параметра.
                        List<User> usersCar = psql.userSearch(id, surname, name, secondName, city);
                        users.addAll(usersCar);
                    }

                }
            }
            psql.close();

            req.setAttribute("usersList", users);
            req.setAttribute("html", html);

        }else{
            req.setAttribute("html","<H3>Не смог подключиться к БД.</H3>");
        }

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("view/search.jsp");
        requestDispatcher.forward(req, resp);


    }

}
