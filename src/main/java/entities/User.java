package entities;

import java.util.Set;

//TODO: Прикрутить lambock, чтобы не плотить геттеры и сеттеры
public class User {


    private long id;
    private String name;
    private String secondName;
    private String surname;
    private String cityName;
    private Set<Car> cars;
    private String password;

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public User(long id, String name, String secondName, String surname, String cityName) {
        this.id = id;
        this.name = name;
        this.secondName = secondName;
        this.surname = surname;
        this.cityName = cityName;
    }


    public User(String name, String password) {
        this.name = name;
        this.password=password;

    }


    //TODO: Подключить lombok, избавиться от кучи геттеров и сеттеров

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getSurname() {
        return surname;
    }


    public String getCityName() {
        return cityName;
    }

    public Set<Car> getCars() {
        return cars;
    }

    public String getCarsTitle() {
        String carsTitle=new String();
        carsTitle="<ul>";

        for(Car car: this.cars) {
           carsTitle+="<li><div style=\"padding: 1px 1px 1px 1px\"><button type=\"button\" class=\"btn btn-xs btn-primary\">"+car.getLicensePlate()+"</button>&nbsp;"+car.getModel()+"<span class=\"badge\">"+car.getYear()+"</span><div> </li>";
        }
        carsTitle+="</ul>";
        return carsTitle;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


    public void setCars(Set<Car> cars) {
        this.cars = cars;
    }

}
