package entities;

import java.util.Date;

public class Car {

    private long id;
    private String licensePlate;
    private String model;
    private int year;
    private long userId;

    public Car(long id, String licensePlate, String model, int year, long userId) {
        this.id = id;
        this.licensePlate = licensePlate;
        this.model = model;
        this.year = year;
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public long getUserId() {
        return userId;
    }


    public void setId(long id) {
        this.id = id;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    //Номер автомобиля является уникальным, поэтому по нему будем высчитывать hashCode
    public int hashCode(){
        return licensePlate.hashCode();
    }

}
