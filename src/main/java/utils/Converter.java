package utils;

public final class Converter {

    public static String ruToLat(String str) {

        if(str.matches("[А,В,Е,К,М,Н,О,Р,С,Т,У,Х,0-9]+$")){//Если русские, то преобразуем, иначе ничего не делаем
            StringBuffer res = new StringBuffer(str.length());

            String[] english = {"A", "B", "E", "K", "M", "H", "O", "P", "C", "T", "Y", "X","0","1","2","3","4","5","6","7","8","9","0"};
            String russian = "АВЕЛМНОРСТУХ01234567890";

            for (int i = 0; i < str.length(); ++i) {
                res.append(english[russian.indexOf(str.charAt(i))]);
            }
            return res.toString();
        }else{
            return str;
        }




    }
}