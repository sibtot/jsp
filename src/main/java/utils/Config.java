package utils;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

public class Config {
    private static String errorText;
    private static int error=0;

    final static Logger logger = Logger.getLogger(Config.class);

    public static Configuration readConfig() {

        try {
            org.apache.commons.configuration.Configuration config = new PropertiesConfiguration("jsp.config");
            return config;
        } catch (Exception e) {
            errorText="Конфигурационный файл не найден, использую значения по-умолчанию";
            org.apache.commons.configuration.Configuration config = new PropertiesConfiguration();

            config.setProperty("server","greenmon.ru");
            config.setProperty("port","5432");
            config.setProperty("dbname","testbase");

            config.setProperty("login", "jsp");
            config.setProperty("password", "Jsp457");

            logger.error(errorText);
            logger.debug(errorText,e);

            return config;
        }

    }

}
