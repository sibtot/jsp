package utils;



import entities.Car;
import entities.User;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import java.sql.*;
import java.util.*;
import java.util.List;


public class Postgres {


    private Connection connection = null;

    Configuration config;
    private String url;
    private String login;
    private String password;

    private String errorText="";
    final static Logger logger = Logger.getLogger(Postgres.class);


    public Postgres() {
        config=Config.readConfig();
        url="jdbc:postgresql://"+config.getString("server")+":"+config.getString("port")+"/"+config.getString("dbname");
        login=config.getString("login");
        password=config.getString("password");
        connect();
    }

    public void connect(){
        try {

            Class.forName("org.postgresql.Driver");
            //Создаём соединение
            DriverManager.setLoginTimeout(5);
            connection = DriverManager.getConnection(url, login, password);

            errorText="Подключился к "+url;
            logger.info(errorText);
            errorText="";

        }catch(Exception e){
                errorText="Не смог подключиться к "+url;
                logger.error(errorText);
                logger.debug(errorText,e);
            }
    }

    public void close(){

        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                errorText="Ошибка закрытия подключения к базе данных "+url;
                logger.error(errorText);
                logger.debug(errorText,e);
            }
        }
    }

    public String getErrorText() {
        return errorText;
    }

    public List<User> userSearch(){

        List<User> users=new ArrayList<>();
        try {
            Statement statement = null;
            statement = connection.createStatement();

            ResultSet result = statement.executeQuery(
                    "SELECT U.id,U.name,U.secondname,U.surname,C.name as cityname FROM users U, cities C " +
                            "where U.city_id=C.id order by U.surname"
            );
            while (result.next()) {
                //name,secondname,surname,city_id
                User user=new User(
                        result.getLong("id"),
                        result.getString("name"),
                        result.getString("secondname"),
                        result.getString("surname"),
                        result.getString("cityname")
                );
                user.setCars(carSearch(user.getId()));//Ищем все автомобили пользователя
                users.add(user);
            }
        }catch(Exception e){
            errorText="Не смог выполнить запрос для userSearch";
            logger.error(errorText);
            logger.debug(errorText,e);
        }
        return users;
    }

    public List<User> userSearch(Long userId,String surname,String name, String secondName,String city){

        List<User> users=new ArrayList<>();

        try {
            PreparedStatement preparedStatement = null;

            preparedStatement = connection.prepareStatement(
                    "SELECT U.id,U.name,U.secondname,U.surname,C.name as cityname " +
                            "FROM users U,cities C WHERE " +
                            "upper(U.surname) like ? and " +
                            "upper(U.name) like ? and " +
                            "upper(U.secondname) like ? and  " +
                            "upper(C.name) like ? and " +
                            "U.id = ? and " +
                            "U.city_id=C.id " +
                            "order by U.surname"
            );

            preparedStatement.setString(1,"%"+surname.toUpperCase()+"%");
            preparedStatement.setString(2,"%"+name.toUpperCase()+"%");
            preparedStatement.setString(3,"%"+secondName.toUpperCase()+"%");
            preparedStatement.setString(4,"%"+city.toUpperCase()+"%");
            preparedStatement.setLong(5,userId);

            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {

                User user=new User(
                        result.getLong("id"),
                        result.getString("name"),
                        result.getString("secondname"),
                        result.getString("surname"),
                        result.getString("cityname")
                );

                user.setCars(carSearch(user.getId()));//Ищем все автомобили пользователя
                users.add(user);
            }

        }catch(Exception e){

            errorText="Не смог выполнить запрос для userSearch("+userId+","+surname+","+name+","+secondName+","+city+")";
            logger.error(errorText);
            logger.debug(errorText,e);

        }
        return users;
    }



    public List<User> userSearch(String surname,String name, String secondName,String city){

        List<User> users=new ArrayList<>();
        try {
            PreparedStatement preparedStatement = null;

            preparedStatement = connection.prepareStatement(
                    "SELECT U.id,U.name,U.secondname,U.surname,C.name as cityname " +
                            "FROM users U,cities C WHERE " +
                            "upper(U.surname) like ? and " +
                            "upper(U.name) like ? and " +
                            "upper(U.secondname) like ? and  " +
                            "upper(C.name) like ? and  " +
                            "U.city_id=C.id " +
                            "order by U.surname"
            );
            preparedStatement.setString(1,"%"+surname.toUpperCase()+"%");
            preparedStatement.setString(2,"%"+name.toUpperCase()+"%");
            preparedStatement.setString(3,"%"+secondName.toUpperCase()+"%");
            preparedStatement.setString(4,"%"+city.toUpperCase()+"%");

            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                //name,secondname,surname,city_id
                User user=new User(
                        result.getLong("id"),
                        result.getString("name"),
                        result.getString("secondname"),
                        result.getString("surname"),
                        result.getString("cityname")
                );
                user.setCars(carSearch(user.getId()));//Ищем все автомобили пользователя
                users.add(user);
            }
        }catch(Exception e){

            errorText="Не смог выполнить запрос для userSearch("+surname+","+name+","+secondName+","+city+")";
            logger.error(errorText);
            logger.debug(errorText,e);

        }
        return users;
    }


    public Set<Car> carSearch(long userId){

        Set<Car> cars=new HashSet<>();

        try {
            PreparedStatement preparedStatement = null;

            preparedStatement = connection.prepareStatement(
                    //id,license_plate,model,year,user_id
                    "SELECT id,license_plate,model,year,user_id from cars where user_id = ?"
            );
            preparedStatement.setLong(1,userId);

            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                Car car=new Car(
                        result.getLong("id"),
                        result.getString("license_plate"),
                        result.getString("model"),
                        result.getInt("year"),
                        result.getLong("user_id")
                );
                cars.add(car);
            }
        }catch(Exception e){
            errorText="Не смог выполнить запрос для carSearch("+userId+")";
            logger.error(errorText);
            logger.debug(errorText,e);
        }
        return cars;
    }


    public Set<Car> carSearchByLicensePlate(String str,String model){

        Set<Car> cars=new HashSet<>();
        str=str.trim();
        model=model.trim();

        try {
            PreparedStatement preparedStatement = null;

            preparedStatement = connection.prepareStatement(
                    //id,license_plate,model,year,user_id
                    "SELECT id,license_plate,model,year,user_id from cars where upper(license_plate) like ? and upper(model) like ?"
            );
            preparedStatement.setString(1,"%"+str.toUpperCase()+"%");
            preparedStatement.setString(2,"%"+model.toUpperCase()+"%");

            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                Car car=new Car(
                        result.getLong("id"),
                        result.getString("license_plate"),
                        result.getString("model"),
                        result.getInt("year"),
                        result.getLong("user_id")
                );
                cars.add(car);
            }
        }catch(Exception e){

            errorText="Не смог выполнить запрос для userSearch("+str+","+model+")";
            logger.error(errorText);
            logger.debug(errorText,e);

        }
        return cars;
    }

    public Set<Car> carSearchByModel(String str){

        Set<Car> cars=new HashSet<>();
        str=str.trim();
        try {
            PreparedStatement preparedStatement = null;
            preparedStatement = connection.prepareStatement(
                    //id,license_plate,model,year,user_id
                    "SELECT id,license_plate,model,year,user_id from cars where upper(model) like ?"
            );
            preparedStatement.setString(1,"%"+str.toUpperCase()+"%");


            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                Car car=new Car(
                        result.getLong("id"),
                        result.getString("license_plate"),
                        result.getString("model"),
                        result.getInt("year"),
                        result.getLong("user_id")
                );
                cars.add(car);
            }
        }catch(Exception e){

            errorText="Не смог выполнить запрос для carSearchByModel("+str+")";
            logger.error(errorText);
            logger.debug(errorText,e);

        }
        return cars;
    }


    public Set<Car> carSearchByYearModel(String year,String model){

        Set<Car> cars=new HashSet<>();
        model=model.trim();

        try {
            PreparedStatement preparedStatement = null;

            preparedStatement = connection.prepareStatement(
                    //id,license_plate,model,year,user_id
                    "SELECT id,license_plate,model,year,user_id from cars where year = ? and upper(model) like ?"
            );
            preparedStatement.setInt(1,Integer.parseInt(year));
            preparedStatement.setString(2,"%"+model.toUpperCase()+"%");

            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                Car car=new Car(
                        result.getLong("id"),
                        result.getString("license_plate"),
                        result.getString("model"),
                        result.getInt("year"),
                        result.getLong("user_id")
                );
                cars.add(car);
            }
        }catch(Exception e){

            errorText="Не смог выполнить запрос для carSearchByModel("+year+","+model+")";
            logger.error(errorText);
            logger.debug(errorText,e);

        }
        return cars;
    }

    public Set<Car> carSearchByYearLicensePlateModel(String year,String licensePlate,String model){

        Set<Car> cars=new HashSet<>();
        licensePlate=licensePlate.trim();
        model=model.trim();

        try {
            PreparedStatement preparedStatement = null;

            preparedStatement = connection.prepareStatement(
                    //id,license_plate,model,year,user_id
                    "SELECT id,license_plate,model,year,user_id from cars where year = ? and upper(license_plate) like ? and upper(model) like ?"
            );
            preparedStatement.setInt(1,Integer.parseInt(year));
            preparedStatement.setString(2,"%"+licensePlate.toUpperCase()+"%");
            preparedStatement.setString(3,"%"+model.toUpperCase()+"%");

            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {
                Car car=new Car(
                        result.getLong("id"),
                        result.getString("license_plate"),
                        result.getString("model"),
                        result.getInt("year"),
                        result.getLong("user_id")
                );
                cars.add(car);
            }
        }catch(Exception e){
            errorText="Не смог выполнить запрос для carSearchByYearLicensePlateModel("+year+","+licensePlate+","+model+")";
            logger.error(errorText);
            logger.debug(errorText,e);

        }
        return cars;
    }


}

