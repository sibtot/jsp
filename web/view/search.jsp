<%@ page import="entities.User" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users</title>

    <script src="resources/js/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/jsp.js"></script>

    <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/css/jsp.css">
</head>

<script type='text/javascript'>
    $("#showcars").click(function(){
        if($('#showcars').html().valueOf()=="Раскрыть данные"){
            $('.collapse').removeClass('out').addClass('in');
            $('#showcars').html("Свернуть данные");
        }else{
            $('.collapse').removeClass('in').addClass('out');
            $('#showcars').html("Раскрыть данные");
        }
    });//showcars

</script>

<body>

<%
    List<User> users= (ArrayList<User>) request.getAttribute("usersList");
    String html= (String) request.getAttribute("html");
%>
<div>
    <button id="close" class="btn btn-info btn-sm" onclick="location.href='/jsp-1.0'">Закрыть</button>
    <% if (users != null && !users.isEmpty()) {%>
    <button id="showcars" class="btn btn-info btn-sm" >Раскрыть данные</button>
    <% }
    if(html.length()>0){
        out.println("<br><h3>"+html+"</h3>");
    }
    %>
</div>

<div>
    <div>


        <%
        if (users != null && !users.isEmpty()) {
        %>

        <div>
            <h3>Пользователи</h3>
        </div>

        <div>
            <table class="table table-hover table-condensed table-sm">
                <thead>
                <tr>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th>Отчество</th>
                    <th>Город</th>
                    <%--<th>Авто</th>--%>
                </tr>
                </thead>

                <tbody>
                <%

                for (User user: users) {
//                    out.println("<tr class=\"default accordion-toggle\" data-toggle=\"collapse\" data-target=\"#row"+user.getId()+"\">");
                    out.println("<tr class=\"default\" >");
                    out.println("<td>"+user.getSurname()+"</td>");
                    out.println("<td>"+user.getName()+"</td>");
                    out.println("<td>"+user.getSecondName()+"</td>");
                    out.println("<td>"+user.getCityName()+"</td>");
                    out.println("</tr>");
                    out.println("<tr>" +
                            "<td class=\"light\" colspan=\"4\">" +
                            "<div id=\"row"+user.getId()+"\" class=\"collapse out\">\n" +
                            user.getCarsTitle()+
                            "</div></td>" +
                            "</tr>");
                }
                %>
                </tbody>

            </table>
        <%
            } else out.println("<H3>Пользователи по вашему запросу не найдены.</H3>");

        %>
        </div>
    </div>
</div>


</body>
</html>