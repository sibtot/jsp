



$(document).ready(function(){


    let closeButton="<button id=\"close\" class=\"btn btn-info btn-sm\" onclick=\"location.href=\'/jsp-1.0\'\">Закрыть</button>";

    //Проверка корректности различных полей
    function checkInputFIO(str){
        var reg=/^[а-яА-ЯёЁa-zA-Z- ]+$/; //Допустимы только буквы, пробелы и тире (могут быть фамилии Ку Ян)
        return reg.test(str);
    }
    function checkInputWithDigets(str){
        var reg=/^[а-яА-ЯёЁa-zA-Z0-9- ]+$/;
        return reg.test(str);
    }

    //Функция проверки входных данных
    function checkInputData(obj){
        let closeButton="<button id=\"close\" class=\"btn btn-info btn-sm\" onclick=\"location.href=\'/jsp-1.0\'\">Закрыть</button>";
        if((obj.name=="")&&(obj.surname=="")&&(obj.secondName=="")&&(obj.city=="")&&(obj.car=="") ){
            $("#result").html(closeButton+"<h3>Для поиска необходимо ввести значение хотя бы в одно поле.</h3>");
            return false;
        }else{
            if((!checkInputFIO(obj.name)) && (obj.name.length>0)){
                $("#result").html(closeButton+"<h3>Не верно указано имя.</h3>");
                return false;
            }else if((!checkInputFIO(obj.surname)) && (obj.surname.length>0)){
                $("#result").html(closeButton+"<h3>Не верно указана фамилия.</h3>");
                return false;
            }else if((!checkInputFIO(obj.secondName)) && (obj.secondName.length>0)){
                $("#result").html(closeButton+"<h3>Не верно указано отчество.</h3>");
                return false;
            }else if((!checkInputFIO(obj.city)) && (obj.city.length>0)) {
                $("#result").html(closeButton+"<h3>Недопустимое название города.</h3>");
                return false;
            }else if((!checkInputWithDigets(obj.car)) && (obj.car.length>0)) {
                $("#result").html(closeButton+"<h3>Недопустимые параметры для поиска автомобиля.</h3>");
                return false;
            }
            //Проверяем на корректность данные
            return true;
        }
    }



    function tooltipOnWrong(element){
        $(element).on('input',function (){
            let str=$(element).val().trim()
            if((!checkInputFIO(str)) && (str.length>0))
            {
                $(".tooltip-method input").attr('title', "В данном поле допустимы только буквы, тире и пробелы.");
                $(element).tooltip('show');
            }else{
                $(element).tooltip('destroy');
            }
        });
    }


    function tooltipOnWrongWithDigits(element){
        $(element).on('input',function (){
            let str=$(element).val().trim()
            if((!checkInputWithDigets(str)) && (str.length>0))
            {
                $(".tooltip-method input").attr('title', "В данном поле допустимы только буквы, цифры, тире и пробелы.");
                $(element).tooltip('show');
            }else{
                $(element).tooltip('destroy');
            }
        });
    }

    tooltipOnWrong("#name");
    tooltipOnWrong("#surname");
    tooltipOnWrong("#secondName");
    tooltipOnWrong("#city");
    tooltipOnWrongWithDigits("#car");//Допустим ввод цифр

    //Выполнить поиск
    $("#search").click(function(){

        let surname=$("#surname").val().trim();
        let name=$("#name").val().trim();
        let secondName=$("#secondName").val().trim();
        let city=$("#city").val().trim();
        let car=$("#car").val().trim();

        let obj={
            name:name,
            surname:surname,
            secondName:secondName,
            city:city,
            car:car
        };
        //Выполняем проверку данных
        // let serialObj = JSON.stringify(obj);

        if(checkInputData(obj)){
            $.ajax({
                url: "search",
                method: "GET",
                async: false,
                data:obj,
                success: function(responseData){
                    $("#result").html(responseData);
                },
                error: function(responseData) {
                    $("#result").html(closeButton+"<h3>Не смог получить данные с сервера</h3>");
                }
            });
        }



    });//search


    $("#listall").click(function(){
        $.ajax({
            url: "search",
            method: "GET",
            async: false,
            success: function(responseData){
                $("#result").html(responseData);
            },
            error: function(responseData) {
                $("#result").html(closeButton+"<h3>Не смог получить список всех пользователей</h3>");
            }
        });

    });//listall

    //Меняем class collapse out на collapse in, чтобы развернуть список
    // $("#showcars").click(function(){
    //     if($('#showcars').html().valueOf()=="Раскрыть данные"){
    //         $('.collapse').removeClass('out').addClass('in');
    //         $('#showcars').html("Свернуть данные");
    //     }else{
    //         $('.collapse').removeClass('in').addClass('out');
    //         $('#showcars').html("Раскрыть данные");
    //     }
    // });//showcars



});

