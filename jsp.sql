--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: car_ids; Type: SEQUENCE; Schema: public; Owner: jsp
--

CREATE SEQUENCE public.car_ids
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.car_ids OWNER TO jsp;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: cars; Type: TABLE; Schema: public; Owner: jsp
--

CREATE TABLE public.cars (
    id integer DEFAULT nextval('public.car_ids'::regclass) NOT NULL,
    license_plate character(64),
    model character(64),
    year bigint,
    user_id bigint
);


ALTER TABLE public.cars OWNER TO jsp;

--
-- Name: city_ids; Type: SEQUENCE; Schema: public; Owner: jsp
--

CREATE SEQUENCE public.city_ids
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.city_ids OWNER TO jsp;

--
-- Name: cities; Type: TABLE; Schema: public; Owner: jsp
--

CREATE TABLE public.cities (
    id integer DEFAULT nextval('public.city_ids'::regclass) NOT NULL,
    name character(64),
    description character(64)
);


ALTER TABLE public.cities OWNER TO jsp;

--
-- Name: user_ids; Type: SEQUENCE; Schema: public; Owner: jsp
--

CREATE SEQUENCE public.user_ids
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_ids OWNER TO jsp;

--
-- Name: users; Type: TABLE; Schema: public; Owner: jsp
--

CREATE TABLE public.users (
    id integer DEFAULT nextval('public.user_ids'::regclass) NOT NULL,
    name character(64),
    secondname character(64),
    surname character(64),
    city_id bigint
);


ALTER TABLE public.users OWNER TO jsp;

--
-- Name: car_ids; Type: SEQUENCE SET; Schema: public; Owner: jsp
--

SELECT pg_catalog.setval('public.car_ids', 19, true);


--
-- Data for Name: cars; Type: TABLE DATA; Schema: public; Owner: jsp
--

COPY public.cars (id, license_plate, model, year, user_id) FROM stdin;
1	M001MO77                                                        	BMW X6                                                          	2017	1
2	X001XX77                                                        	Mercedes-Benz GLC 300                                           	2018	3
3	X002XX77                                                        	Mercedes-Benz C238                                              	2017	3
4	X003XX77                                                        	Range Rover Sport                                               	2016	3
5	Y555XX77                                                        	Porsche Panamera S                                              	2015	4
6	K555KK177                                                       	Porsche Cayenne                                                 	2018	4
7	Y238XO54                                                        	Lada Sedan                                                      	2005	5
10	T054OT54                                                        	Citroen C4                                                      	2017	7
11	M136AT36                                                        	УАЗ Патриот                                                     	2014	8
12	M125EP25                                                        	Toyota Crown                                                    	2014	9
13	M125EP125                                                       	Toyota Celica                                                   	2014	9
14	P007PP159                                                       	BMW X5                                                          	2010	10
15	У007КP38                                                        	Toyota Celica                                                   	2001	11
16	P005OT22                                                        	Lada Sedan                                                      	2015	12
17	P005OM22                                                        	УАЗ Патриот                                                     	2015	13
18	K001MM24                                                        	Honda Accord                                                    	2005	14
19	K555MM24                                                        	Subaru Impreza                                                  	2007	14
8	O0780O78                                                        	Volvo XC60                                                      	2005	6
9	A078AA78                                                        	Volvo V40                                                       	2017	6
\.


--
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: jsp
--

COPY public.cities (id, name, description) FROM stdin;
1	Новосибирск                                                     	мой город                                                       
2	Москва                                                          	столица                                                         
3	Санкт-Петербург                                                 	это же Ленинград                                                
4	Ростов-на-Дону                                                  	город папа                                                      
5	Владивосток                                                     	далеко                                                          
6	Пермь                                                           	это город                                                       
7	Красноярск                                                      	столбы                                                          
8	Барнаул                                                         	почти Алтай                                                     
9	Горно-Алтайск                                                   	за ним горы                                                     
10	Иркутск                                                         	город                                                           
\.


--
-- Name: city_ids; Type: SEQUENCE SET; Schema: public; Owner: jsp
--

SELECT pg_catalog.setval('public.city_ids', 10, true);


--
-- Name: user_ids; Type: SEQUENCE SET; Schema: public; Owner: jsp
--

SELECT pg_catalog.setval('public.user_ids', 15, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: jsp
--

COPY public.users (id, name, secondname, surname, city_id) FROM stdin;
1	Андрей                                                          	Феофанович                                                      	Греков                                                          	2
3	Юрий                                                            	Венедиктович                                                    	Пронин                                                          	2
4	Виктор                                                          	Харитонович                                                     	Андреев                                                         	2
5	Семён                                                           	Васильевич                                                      	Слепцов                                                         	1
6	Аркадий                                                         	Петрович                                                        	Паровозов                                                       	3
7	Эдуард                                                          	Владимирович                                                    	Мостовых                                                        	1
8	Сергей                                                          	Михайлович                                                      	Ковалёв                                                         	4
9	Михаил                                                          	Сергеевич                                                       	Подушкин                                                        	5
10	Дмитрий                                                         	Владимирович                                                    	Нестеров                                                        	6
11	Сергей                                                          	Михайлович                                                      	Ковалёв                                                         	10
12	Сергей                                                          	Михайлович                                                      	Расторопшин                                                     	9
13	Сергей                                                          	Михайлович                                                      	Ковалёв                                                         	8
14	Виктор                                                          	Михайлович                                                      	Ковалёв                                                         	7
15	Андрей                                                          	Михайлович                                                      	Безлошадный                                                     	1
\.


--
-- Name: cars cars_pkey; Type: CONSTRAINT; Schema: public; Owner: jsp
--

ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_pkey PRIMARY KEY (id);


--
-- Name: cities cities_pkey; Type: CONSTRAINT; Schema: public; Owner: jsp
--

ALTER TABLE ONLY public.cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: jsp
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: cars cars_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jsp
--

ALTER TABLE ONLY public.cars
    ADD CONSTRAINT cars_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: users users_city_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: jsp
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_city_id_fkey FOREIGN KEY (city_id) REFERENCES public.cities(id);


--
-- PostgreSQL database dump complete
--

